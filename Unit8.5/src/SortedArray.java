
public class SortedArray {

	private int[] array;
	private int numElements;

	public SortedArray(int longArr) {
		array = new int[longArr];
		numElements = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = -1;
		}
	}
	public SortedArray(int longArr, boolean r) {
		array = new int[longArr];
		numElements = longArr;

		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
	}

	public void put(int number) {
		int n = number;
		int n2 = -1;
		int i = 0;
		while (number <= array[i] || array[i] == -1) {
			if (array[i] == -1) {
				array[i] = n;
				numElements++;
				break;
			} else {
				if (n < array[i]) {
					n2 = array[i];
					array[i] = n;
					n = n2;

				}
			}
			i++;
		}
	}

	public void putProfe(int n) {
		int i = 0;
		while (i < numElements && array[i] < n) {
			i++;
		}
		// i has now the position in which to insert
		for (int j = numElements; j > i; j--) {
			if (j < array.length) {
				array[j] = array[j - 1];
			}
		}
		if (i < array.length) {
			array[i] = n;
		}
		if (numElements < array.length) {
			numElements++;
		}
	}

	@Override
	public String toString() {
		String s = "";
		boolean first = true;
		int i = 0;
		while (array[i] != -1) {
			if (first) {
				s += "" + array[i];
				first = false;
			} else {
				s += "," + array[i];
			}
			i++;
		}
		return s;
	}

	public int getNunElements() {
		return numElements;
	}

	public int getSize() {
		return array.length;
	}

	public int getElementAt(int number) {
		return array[number];
	}

	public void removeElement(int n) {
		if(n>= numElements) {return ;}
		for (int i = n; i < numElements - 1; i++) {
			array[i] = array[i + 1];
		}

		array[numElements - 1] = -1;
		numElements--;

	}

	public boolean isEmpty() {
		return (array[0] != -1);
	}

	public boolean isFull() {
		return (array[getSize() - 1] != -1);
	}
	
	public boolean existElement(int n) {
		int i=0;
		while(i< numElements && array[i]< n) {
			
			i++;
		}
		if(i< numElements && array[i]==n) {
				return true;
			}else {
				return false;
			}
		
	}
	public boolean existElement2(int n) {
		return binarySearch(n, 0, numElements-1);
	}
	
	public boolean binarySearch(int n,int minorIndex,int upperIndex) {
		int middle = ((minorIndex + upperIndex)/2);
		if(minorIndex>upperIndex) {
			return false;
		}else if(array[middle]==n) {
			return true;
		}else {
			if(n<array[middle]) {
				return binarySearch(n, minorIndex, middle-1);
			}else {
				return binarySearch(n, middle+1, upperIndex);
			}
		}
	}

}
