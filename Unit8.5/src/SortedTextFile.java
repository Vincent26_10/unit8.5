import java.io.*;
import java.util.*;

public class SortedTextFile {

	public static final String TMP = "tmp";
	private String nameFile;
	private File f;
	private File creator;

	public SortedTextFile(String nameFile) throws IOException {
		this.nameFile = nameFile;
		f = new File(TMP);
		creator = new File(nameFile);

	}

	public String getName() {
		return nameFile;
	}

	public static void merge(String name1, String name2, String exitFile) throws IOException {
		BufferedReader in = null;
		BufferedReader in2 = null;
		PrintWriter out = null;
		String line1;
		String line2;

		
		try {
			in = new BufferedReader(new FileReader(name1));
			in2 = new BufferedReader(new FileReader(name2));
			out = new PrintWriter(new FileWriter(exitFile));
			line1 = in.readLine();
			line2 = in2.readLine();

			while (line1 != null || line2 != null) {
				if (line1 == null) {
					out.println(line2);
					line2 = in2.readLine();
				} else {

					if (line2 == null) {
						out.println(line1);
						line1 = in.readLine();
					}else {
						if(line1.compareTo(line2)<0) {
							out.println(line1);
							line1 = in.readLine();
						}else {
							out.println(line2);
							line2=in2.readLine();
						}
					}
				}
			}

		} finally {
			if (in != null) {
				in.close();
			}
			if (in2 != null) {
				in2.close();
			}
			if (out != null) {
				out.close();
			}
		}

	}

	public boolean existsElement(String s) throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(nameFile));
			String line = "";
			while ((line = in.readLine()) != null) {
				if (line.equalsIgnoreCase(s)) {
					return true;
				}
			}
			return false;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public void print() throws IOException {

		BufferedReader input = null;
		try {
			input = new BufferedReader(new FileReader(nameFile));
			String line;
			while ((line = input.readLine()) != null) {
				System.out.println(line);
			}
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	public static void print(String name) throws IOException {
		SortedTextFile f = new SortedTextFile(name);
		f.print();
	}

	public int getNumElements() throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(nameFile));

			int counter = 0;
			while (in.readLine() != null) {
				counter++;
			}
			return counter;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public String getElementAt(int number) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameFile));
		String s = null;
		try {
			for (int i = 1; i <= number; i++) {
				s = in.readLine();
			}
			return s;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public void emptyFile() {
		f.renameTo(new File(nameFile));
	}

	public boolean isEmpty() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameFile));
		try {
			return in.readLine() == null;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public void removeElementAt(int number) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameFile));
		PrintWriter out = new PrintWriter(new FileWriter(f));
		String line;
		try {
			int count = 1;
			while ((line = in.readLine()) != null) {
				if (count != number) {
					out.println(line);
				}
				count++;
			}
			f.renameTo(new File(nameFile));
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public void put(String text) throws IOException {

		boolean lineWritten = false;
		String s1;

		BufferedReader in = new BufferedReader(new FileReader(creator));
		PrintWriter out = new PrintWriter(new FileWriter(f));

		try {

			while ((s1 = in.readLine()) != null) {
				if (text.compareTo(s1) < 0 && !lineWritten) {
					out.println(text);
					lineWritten = true;
				}
				out.println(s1);
			}
			if (!lineWritten) {
				out.println(text);
			}
			f.renameTo(new File(nameFile));
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}

	}
}
